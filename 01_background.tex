\documentclass[compress,table,aspectratio=169]{beamer}

\input{lecture-style.tex}

\lecture[1]{Background}

\date{2019-07-10}

\begin{document}

\begin{frame}
\maketitle
\end{frame}		


\begin{frame}
\frametitle{Outline}
\tableofcontents[pausesections]
\end{frame}

\section{Probability Theory}
\begin{frame}
\frametitle{Probability Theory}
 In \alert{Probability Theory} we design \alert{experiments} which underly some randomness and ask for the probability of certain outcomes.
\begin{example}[throwing a six-sided die]
The set of all possible values is $\Omega := \{ 1, \dots , 6 \}$. We assume, there is no number to occur more likely than any other. Thus, we design a \alert{probability measure} $\IP$, which assigns a probability of $\frac{1}{6}$ to each possible event in $\Omega$. Thus, its \alert{density} is the constant function $f(\omega) \equiv \frac{1}{6}$ on $\Omega$.
\end{example}
\end{frame}

\subsection{Distributions}
\begin{frame}[fragile]
\frametitle{Distributions}
These probability measures are, in a more general sense, \alert{distributions}. There is an unlimited number of possible distributions and families of distributions. 
\begin{example}
\begin{itemize}
\item<1-> Normal distribution $N(\mu,\sigma^2)$ on $\setR$ with mean $\mu \in \setR$, standard deviation $\sigma \in (0,\infty)$ and density $f_N(x)=\frac{1}{\sqrt{2\pi}\sigma}\exp(\frac{-(x-\mu)^2}{2\sigma^2})$
\item<1-> Uniform distribution $U(a,b)$ on $(a,b) \subset \setR$ (or $[a,b]$) with density $f_U(x)=\frac{1}{b-a}$
\item<1-> Exponential distribution $Exp(\lambda)$ on $[0,\infty)$ with parameter $\lambda \in (0,\infty)$ and density $f_E(x)=\lambda \exp (-\lambda x)$
\end{itemize}
\end{example}
\end{frame}

\begin{frame}
\frametitle{pdf  \& cdf}
Each distribution has besides its density $f$ (\alert{pdf} = probability distribution function) a cumulative distribution function $F$ (\alert{cdf}) that is for continuous distributions
$$ F(x) := \int\limits_{-\infty}^{x} f(t) dt$$
and in case of discrete probability distributions
$$ F(x) := \sum\limits_{\omega \in \Omega, \omega \leq x} f(\omega). $$
\end{frame}

\begin{frame}
\frametitle{Properties of the cdf}
We derive the following properties from the design\\ of a cdf $F$:
\begin{itemize}
\item<1-> $F$ is monotone non-decreasing
\item<1-> $\lim\limits_{x \rightarrow -\infty} F(x) = 0$
\item<1-> $\lim\limits_{x \rightarrow +\infty} F(x) = 1$
\item<1-> $F$ is right-continuous: $\lim\limits_{x \rightarrow +x_0} F(x) = F(x_0)$
\end{itemize}
A pdf or cdf uniquely defines a probability distribution.
\end{frame}

\begin{frame}
\frametitle{distributions in R}
\begin{center}
\begin{tabular}{cc}
distribution &  R command \\
\hline
$\beta(a, b)$ & beta \\
$C(a, b)$ & cauchy \\
$\chi^2$ & chisq \\
Exponential & exp \\
F(m; n) & f \\
$\Gamma$ & gamma \\
$U(a, b)$ & unif\\
lognormal & lnorm \\
$N(\mu,\sigma^2)$ & norm \\
t & t \\
$B(n, p)$ & binom\\
geometric & geom \\
hypergeom. & hyper \\
$NB(\alpha, \theta)$ & nbinom\\
Poisson & pois
\end{tabular}
\end{center}
\end{frame}

\subsection{Random Variables}
\begin{frame}[fragile]
\frametitle{Random Variables}
Formally speaking, a (real valued) \alert{random variable} $Z$ is a (measurable) function $Z:\Omega \rightarrow \setR$. \\ \vspace{8pt}

Practically speaking, a \alert{random variable} $Z$ describes some property which is subject to some randomness. \\ \vspace{8pt}

We usually deal with a \alert{spatial random field} $\mathcal{Z}$ over some spatial region $S$. A spatial random field is a set $\mathcal{Z}:=\{Z(s) | s \in S \}$ of (spatial) random variables $Z(s)$ at different locations $s \in S$. \\ \vspace{8pt}

These $Z(s)$ take for some random event ($\omega \in \Omega$) a value in their domain (e.g. range of temperatures). Their behaviour {\em may} change for different locations $s \in S$ (\alert{non-stationary} vs. \alert{stationary} spatial random fields).
\end{frame}

\subsection{Extreme Value Distributions}
\begin{frame}
\frametitle{Extreme Value Distributions}
A special class of probability distributions are the \alert{extreme value distributions} (\alert{evd}). A evd is characterized by its cdf $G$ through the fact that
$$\lim\limits_{n \rightarrow \infty} G^n = G.$$
Where $G^n$ is the cdf of $\max(X_1, \dots, X_n)$ and the $X_i$ are independent copies of random variables following the distribution induced by $G$.

One can show that there are only three families of extreme value distributions: \alert{Gumbel}, \alert{Fr{\'e}chet} and \alert{Weibull}. These can even be parametrized as single family (\alert{generalized extreme value distribution}).
\end{frame}

\begin{frame}
\frametitle{Properties of evd}
The remarkable difference is in the tails of the distributions. \\ \vspace{12pt}

Extreme Value Distributions posses \alert{heavy tails}. That is, even small regions very far from the mean of the distribution (multiple standard deviations)  have a considerable positive probability.
\end{frame}

\begin{frame}[fragile]
\frametitle{Normal vs. Gumbel}
Comparing the probabilities of the standard Normal and Gumbel distribution both with mean $0$ and standard deviation $1$ the ratios for the first $10$ multiples of the standard deviation are: \\ 
\begin{center}
{\footnotesize \begin{tabular}{c|c}
\rowcolor{blue52n}
x & Gumbel/\-Normal\\
 1 & 0.6 \\
 2 & 1 \\
 3 & 3 \\
 4 & 29 \\
 5 & 746 \\
 6 & 51e+3 \\
 7 & 9e+6 \\
 8 & 4e+9 \\
 9 & 7e+12 \\
10& 25e+15 \\
$\vdots$ & $\vdots$
\end{tabular}}
\end{center}
\end{frame}
% 
% \begin{frame}
% \frametitle{Gumbel}
% The Gumbel evd is defined on $\setR$ with parameters $\mu \in \setR$ and $\beta \in (0,\infty)$. Its density is given by
% $$ f_G (x) = \frac{ e^{-(x-\mu)/\beta}}{\beta} \cdot \exp(-e^{-(x-\mu)/\beta}) $$ and its cumulative distribution function is
% $$ F_G(x) = \exp(-e^{-(x-\mu)/\beta}). $$
% \end{frame}
% 
% \begin{frame}
% \frametitle{Fr{\'e}chet}
% The Fr{\'e}chet evd is defined on $(0,\infty)$ with parameters $\alpha \in (0,\infty)$, $m \in \setR$ and $s \in (0,\infty)$. Its density is given by
% $$ f_F (x) = \alpha \left( \frac{x-m}{s} \right)^{-(1+\alpha)} \exp\left(-\left( \frac{x-m}{s} \right)^{-\alpha}\right) $$ and its cumulative distribution function is
% $$ F_F(x) =  \exp\left(-\left( \frac{x-m}{s} \right)^{-\alpha}\right). $$
% \end{frame}
% 
% \begin{frame}
% \frametitle{Weibull}
% The Weibull evd is defined on $[0,\infty)$ with parameters $\lambda \in (0,\infty)$ and $k \in (0,\infty)$. Its density is given by
% $$ f_W (x) = \frac{k}{\lambda}\left( \frac{x}{\lambda} \right)^{k-1} \cdot \exp\left(-\left( \frac{x}{\lambda} \right)^k\right) $$ and its cumulative distribution function is
% $$ F_W(x) = 1-\exp\left(-\left( \frac{x}{\lambda} \right)^k \right). $$
% \end{frame}

\begin{frame}
\frametitle{The three Extreme Value Distributions}
\includegraphics[width=102mm]{figures/evd_densities.png}
\end{frame}

\subsection{cdf-Transform}
\begin{frame}
\frametitle{cdf-Transform}
We will need the following theorem later on:
\begin{theorem}
For a random variable $X$ following any distribution $D$ with cdf $F_D$ the transformed variable $F_D(X)$ follows the standard uniform distribution $U(0,1)$.
\end{theorem}
\end{frame}

\subsection{n-variate Distributions}
\begin{frame}[fragile]
\frametitle{from univariate to n-variate Distributions}
There are several ways of extending univariate distributions to n-variate distributions (explicit, conditional, copulas, \dots). Consider for example the n-variate normal distribution:
\begin{example}
We define new parameters: the mean vector ${\bm \mu} \in \setR^k$ and the covariance matrix $\Sigma \in \setR^k \times \setR^k$ (symmetric and positive-definite). The multivariate density is given by
$$ f_N({\bm x}) = \frac{1}{\sqrt{\det(2 \pi \Sigma)}} \exp\left( -\frac{1}{2} (\bm x - \bm \mu)^t \Sigma^{-1}(\bm x - \bm \mu) \right).$$
\end{example}
\end{frame}

\begin{frame}
\frametitle{The Joint cdf}
The \alert{joint cdf} of some n-variate distribution $\IP_n$ is given by:
$$H(x_1, \hdots, x_n) = \IP_n(X_1 \leq x_1, \hdots, X_n \leq x_n) $$
In the case of bivariate distributions it is the probability of the lower left rectangle of the point $(x_1,x_2)$\\ (i.e. $(-\infty,x_1) \times (-\infty,x_2)$).

\end{frame}

\section{Statistics}
\begin{frame}
\frametitle{Statistics}
In (geo)statistics we usually observe a \alert{sample}/\alert{realization} $\sZ=\big(z(s_1), \hdots, z(s_n)\big)$ of some spatial random field $\mathcal{Z}=\{ Z(s_i) | 1 \leq i \leq n \}$ and want to derive the underlying probability distribution. \\ \vspace{8pt}

Knowing the underlying probabilistic model enables us to explain the observed processes, to run simulations and to perform forecasts.
\end{frame}

\subsection{Estimation}
\begin{frame}[allowframebreaks]
\frametitle{Single Characteristic Values}
Usually we start with (descriptive) estimates of single values like \alert{mean}, \alert{variance} or \alert{skewness} of a given sample. \\ \vspace{8pt} 

In some cases, these (moment) estimates completely determine the {\em parameters} of a distribution (but not the distribution itself!). This estimation procedure is called \alert{method of moments}.

\framebreak

\begin{example}
The parameter $\lambda$ of the exponential distribution $Exp(\lambda)$ can be estimated by the mean $\hat{\mu}$ of the sample $\sZ$ through $\hat{\lambda}(\sZ) = \hat{\mu}^{-1}$.
\end{example}

The skewness of a sample may give advise in selecting a suitable distribution before one estimates the parameters. 
\end{frame}

\begin{frame}
\frametitle{Method of Moments}
The empirical (according to the sample $\sZ=(z_1, \hdots, z_n)$) and theoretical (of the distribution $Z$) moments of different order are compared. This leads to a set of equations which can then be solved for the distributions parametrs:
\begin{align*}
\IE(Z) &= \frac{1}{n} \sum\limits_{i=1}^{n} z_i \\
\IE(Z^2) &=  \frac{1}{n} \sum\limits_{i=1}^{n} z^2_i \\
\Var(Z) = \IE(Z^2) - \IE(Z)^2 &= \frac{1}{n} \sum\limits_{i=1}^{n} z^2_i - \big( \frac{1}{n} \sum\limits_{i=1}^{n} z_i\big)^2 \\
\IE(Z^3) &=  \frac{1}{n} \sum\limits_{i=1}^{n} z^3_i
\end{align*}
\end{frame}

\begin{frame}[fragile]
\frametitle{Maximum Likelihood}
The \alert{maximum likelihood} approach grounds on the assumption that the drawn sample $\sZ$ is a very likely one. Thus, the parameter(s) $\theta \in \Theta$ are chosen for which $\sZ$ becomes the {\em most likely} sample to occur:
$$ \hat{\theta}_M(\sZ)= \arg\max_{\theta \in \Theta} \prod\limits_{i=1}^{n} f_\theta(z) $$
or equivalently
$$ \hat{\theta}_L(\sZ)= \arg\max_{\theta \in \Theta} \sum\limits_{i=1}^{n} \log\big(f_\theta(z)\big) $$
\end{frame}

\begin{frame}
\frametitle{Bayes Estimator}
We add an \alert{a priori} belief $p:\Theta \rightarrow [0,1]$ to the parameter space $\Theta$ (no clue might mean uniform distribution over all parameters). The \alert{Bayes estimator} minimizes the mean square error (MSE, or any other appropriate risk). Thus $\theta$ becomes a random variable. \\
For the MSE the Bayes estimator $\hat{\theta}_B(\sZ)$ is the mean of the \alert{a posteriori} distribution of $\theta$ and given by
$$ \hat{\theta}_B(\sZ) = \IE(\theta|\sZ) = \int\limits_{\Theta} \vartheta f(\vartheta|\sZ) d\vartheta$$
where 
$$ f(\vartheta|\sZ) =_{Bayes} \frac{p(\vartheta) f_\vartheta(\sZ)}{\int\limits_{\Theta} p(t) f_t(\sZ) dt}.$$
\end{frame}

\begin{frame}[fragile]
\frametitle{Tools in R}
\begin{itemize}
\item \verb?mean?, \verb?var?, \verb?sd?, \verb?summary?
\item histograms: \verb?hist(data, freq=F)?
\item overlaying functions: \verb?curve(dnorm(x), add=T)? 
\item empirical cdf: \verb?ecdf(data)? returns a step function
\item quantile-quantile plots: \verb?qqplot? compares quantiles of two samples
\item calculating quantiles: \verb?quantile(data, prob)?
\item log-likelihood estimation: \verb?mle(minuslogl)? minimizes the \verb?minuslogl? function
\item estimating a non parametric density: \verb?density(data, bw, kernel, ...)?
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Fitting Extreme Value Distributions}
We can either assume that the sample is drawn from an evd alone or a convex combination of distributions. An evd can be fitted in the same way as {\em usual} distributions, but there are some more tools. 
\begin{itemize}
\item<1-> the POT-method (points over threshold) helps to select a threshold and to identify the maxima (gpd)
\item<1-> annual block method: select the daily/weekly/monthly/yearly maxima as sample (gev)
\end{itemize}
\end{frame}

\begin{frame}[fragile, allowframebreaks]
\frametitle{Fitting a evd Using POT}
We use the dataset \verb?portpirie? (package: \verb?evd?) containing the annual maximum sea levels from 1923 to 1987. 
\begin{figure}
\includegraphics[width=102mm]{figures/hist_portpirie.png}
\end{figure}

\framebreak
The \alert{mean residual life function} 
$$ mrl(u) := \IE(Z-u | Z > u) = \frac{\IE\big((Z_i-u)\bm1_{Z_i > i}\big)}{\IP(Z_i > u)} $$ is linear in $u$ for \alert{generalized pareto distributions} (a joint parametrization of evd for exceedances). \\ \vspace{8pt}

Plotting $mrl(u)$ for all possible values lets us choose an appropriate threshold. The slope $b$ of the plot can be used to calculate the \alert{shape} parameter $\gamma$ by $\hat{\gamma} = b/(1+b)$. \\ \vspace{8pt}

The parameters \alert{scale} $\sigma$ and \alert{location} $\mu$ can be derived from a qq-plot of theoretical quantiles of a gpd with $\gamma=\hat\gamma$, $\sigma=1$ and $\mu=0$ against the sample.

\framebreak

We chose a threshold of 3.85~m and fitted a line with slope $b=-0,2$. \vspace{-12pt}
\begin{figure}
\includegraphics[width=98mm]{figures/mrl_portpirie.png}
\end{figure}
{\footnotesize
\begin{verbatim}
mrlplot(portpirie)
abline(v=3.85,col="red")
abline(1.01,-0.2,col="red")
\end{verbatim}
}
\framebreak

A linear model gave $\hat{\sigma}\approx0.32$ and $\hat{\mu}\approx3.84$.
\begin{figure}
\includegraphics[width=98mm]{figures/qqplot_portpirie.png}
\end{figure}
{\footnotesize
\begin{verbatim}
Call:
lm(formula = sort(portpirie[portpirie > 3.85]) ~ 
               qgpd((1:44)/45, shape = -0.25))
Coefficients: (Intercept)   slope
                   3.8431  0.3231 
\end{verbatim}
}

\framebreak
\begin{figure}
\includegraphics[width=102mm]{figures/evd_fit.png}
\end{figure}
\end{frame}

\begin{frame}[fragile,allowframebreaks]
\frametitle{Fitting a evd Using Anual Blocks}
The data set portpirie actually is aquired as anual maxima water levels. Thus, we might try to fit a \alert{generalized extreme value distribution} (\alert{gev}) right away. 
{\footnotesize
\begin{verbatim}
> fgev(portpirie)
Call: fgev(x = portpirie) 
Deviance: -8.678117 

Estimates
     loc     scale     shape  
 3.87475   0.19805  -0.05012  

Standard Errors
    loc    scale    shape  
0.02793  0.02025  0.09826  

Optimization Information
  Convergence: successful 
  Function Evaluations: 31 
  Gradient Evaluations: 8 
\end{verbatim}}

\framebreak

A visual comparison shows a quite reasonable fit:
\begin{figure}
\includegraphics[width=102mm]{figures/gev_portpirie.png}
\end{figure}

\begin{figure}

\end{figure}
\end{frame}

\subsection{GOF}
\begin{frame}
\frametitle{Goodness of Fit Testing}
Most techniques will give an estimate no matter how far fetched the underlying concept might be. \alert{Goodness of fit tests} (\alert{GOF}) are used to evaluate the fit. \\ \vspace{8pt}

Depending on the complexity of the model, a simple test-statistic might do, or a simulation needs to be run in order to compare the behavior of the fitted probability distribution to the one sample.
\end{frame}

\begin{frame}[fragile, allowframebreaks]
\frametitle{Comparing Distributions}
The \alert{Kolmogorov-Smirnov test} (\alert{KS-test}) compares the ecdf with the fitted cdf by calculating its biggest difference. An alternative is the \alert{Cram{\'e}r-von-Mises test} (\alert{CvM-test}) which considers the integral over the squared difference of two cdfs.

\framebreak 

\begin{example}
In case of the fitted gpd we get:
{\footnotesize
\begin{verbatim}
> ks.test(exce,"pgpd",loc=3.8431,scale=0.3231,shape=-0.2)

        One-sample Kolmogorov-Smirnov test

data:  exce 
D = 0.0753, p-value = 0.9644
alternative hypothesis: two-sided 
\end{verbatim}}
Thus, our hypothesis (the sample was drawn from gpd with the estimated parameters) can not be rejected for any common significance level.
\end{example}

\framebreak
\begin{example}
In case of the automatic fit of the gev we get:
{\footnotesize
\begin{verbatim}
> ks.test(portpirie, "pgev", loc=3.87475, scale=0.19805,
                                            shape=-0.05012)

        One-sample Kolmogorov-Smirnov test

data:  portpirie 
D = 0.0606, p-value = 0.9706
alternative hypothesis: two-sided 
\end{verbatim}}
Thus, our hypothesis (the sample was drawn from gev with the estimated parameters) can not be rejected for any common significance level.
\end{example}

\end{frame}

% \begin{frame}
% \frametitle{GOF by simulation}
% In cases where the underlying distribution used to calculate critical values/p-values is unknown a bootstrapping procedure can be used:
% \begin{enumerate}
% \item<1->
%  estimate $\theta$ from the sample $\sZ$ through one of the given estimators
% \item<1-> 
%  compute the test-statistic $s_0$ (e.g. CvM-test-statistic)
% \item<1->
%  simulate a sample $\bar{\sZ}$ from the assumed distribution of the same size as the original one
% \item<1->
%  estimate $\bar{\theta}$ from $\bar{\sZ}$ through the same estimator as above
% \item<1->
%  repeat the steps 3 and 4 for a large integer $N>100$ and compute each test-statistic $s_i, 1 \leq i \leq N$
% \item<1->
%  the p-value can be approximated by $\#\{ 1 \leq i \leq N \ | \ s_i > s_o \}/N.$
% \end{enumerate}
% 
% \end{frame}

\subsection{Practical}

\begin{frame}[fragile]
\frametitle{Tasks}
\begin{itemize}
\item fit some distributions (i.e. evd) to given samples (meuse, or use \verb?data()? to look for more) using the methods of moments and the maximum likelihood estimator
\item fit a distribution to the meuse dataset/your dataset
\end{itemize}
\end{frame}


% \subsection{Practical}
% \begin{frame}
% \frametitle{Tasks}
% \begin{itemize}
% \item<1-> play around with some distributions (i.e. evd) by comparing densities for different parameters of one family and between families
% \item<1-> find parameters for the Gumbel distribution such that its mean and standard deviation are (approximately) the same as for the standard normal distribution.
% \item<1-> empirically validate the theorem on the cdf-Transform
% \end{itemize}
% \end{frame}
% 

% \section{References \& further readings}
% \begin{frame}[fragile]
% \begin{thebibliography}{}
% \bibitem[Statistik]{Statistik}Löwe, M., Mathematische Statistik \url{http://wwwmath.uni-muenster.de/statistik/loewe/mathstatistik.pdfl}
% 
% \bibitem[EVT]{EVT}Löwe, M., Extremwert Theorie, \url{http://wwwmath.uni-muenster.de/statistik/loewe/extrem.pdf}
% 
% \end{thebibliography}
% \end{frame}

\end{document}