\documentclass[compress,table,href,aspectratio=169]{beamer}

\usepackage{animate}

\input{lecture-style.tex}

\lecture[3]{Spatial and Spatio-Temporal Copulas}

\date{2019-07-10}

\graphicspath{{figures/}}

\begin{document}

\begin{frame}
\maketitle
\end{frame}		

\section{Problem}
\begin{frame}{Interpolation of spatio-temporal data}
\begin{columns}[t]
\begin{column}{0.45\textwidth}<1->
Spatio-temporal data is typically given at a set of discrete locations $(s_i, t_i) \in S \times T$.
\end{column}
\begin{column}{0.45\textwidth}<1->
For a continuously spreading process, a full spatio-temporal random field $Z$ modelling the process at any location $(s,t) \in S\times T$ is typically desired.
\end{column}
\end{columns}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{interpolation.pdf}
\end{figure}
\end{frame}

\begin{frame}{Possible solutions}
\begin{enumerate}[i.]
\item IDW - inverse distance weighted average
\item A multivariate Gaussian distribution is assumed where
\begin{itemize}
\item<2-> a variogram function parametrizes the (large) covariance matrix by distance
\item<2-> the mean vector is set to the observed values
\end{itemize}
\item \dots
\item A probabilistic local approach based on vine copulas where
\begin{itemize}
\item<2-> the bivariate buildings blocks are parametrized by distance
\item<2-> the mean is determined by the marginal distribution
\end{itemize}
\item oter copula approaches e.g.\, by A. Bardossy, T. Erhardt or H. Kazianka
\end{enumerate}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Motivation}

\begin{frame}{Why bother? - Effects of different spatial copulas:}
\vspace{-12pt}
\begin{center}
\includegraphics[width=0.6  \textwidth]{ellipSymWireframe.pdf}
\end{center}
\end{frame}

\subsection{Multivariate Copulas}

\begin{frame}{Hierarchical Copulas}
\begin{figure}
\center
\includegraphics[width=0.5\textwidth]{hierarchical_kendall.png}
\end{figure}
\end{frame}

\begin{frame}{Vine Copulas}

Vine Copulas are based on the pair-copula construction \cite{Aas2009} and allow to build multivariate copulas out of bivariate building blocks.
\begin{figure}
\center
\includegraphics[width=0.8\textwidth]{d-vine}
\end{figure}
\end{frame}

\begin{frame}{Objective}
\begin{center}
\Large\alert{How can vine copulas contribute to the modelling of temporal, spatial and spatio-temporal phenomena?}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Vine Copulas}

\subsection{Time Series}
\begin{frame}[allowframebreaks]{Multivariate return periods}
Simulated trivariate time series of annual maximum peak discharge with corresponding volume and duration.
\begin{figure}
\center
\includegraphics[width=0.5\textwidth]{comp_JRP}
\end{figure}

\begin{figure}
\center
\includegraphics[width=\textwidth]{overview}
\end{figure}
\end{frame}

\subsection{Single tree} % spatial and spatio-temporal
\begin{frame}{Basic set-up \& assumptions}
%Thinking of pairs of locations $(s_1,t_1)$, $(s_2,t_2)$ we assume \dots
\begin{description}
\item[spatio-temporal process] $Z:\Omega\times S \times T \rightarrow \IR$\\
with margins $F_{(s,t)}$
\item[distance] has a strong influence on the strength of dependence between locations
\item[dependence structure] is identical for all neighbours, but might change with distance
\item[stationarity] on the copula scale and build $k$ lag classes by spatial distance for each temporal distance $\Delta$,\\
estimate a bivariate copula $c^\Delta_{j}(u,v)$ for all lag classes $$\big\{[0,b_1),[b_1,b_2), \dots, [b_{k-1},b_k)\big\}\times\big\{0,\dots,\Delta_{\rm max}\big\}$$
\end{description}
\end{frame}

\begin{frame}{Accounting for distance in the bivariate copulas}
The \alert{bivariate spatial copula} density is a convex combination of bivariate copula densities:

{\small
$$ c^\Delta_{h} (u,v) := \left\{
\begin{array}{ll}
c_{1,h}(u,v) &, 0 \leq h < l_1 \\
(1-\lambda_2) c_{1,h}(u,v) + \lambda_2 c_{2,h}(u,v) &, l_1 \leq h < l_2 \\
\vdots & \vdots \\
(1-\lambda_k) c_{k-1,h}(u,v) + \lambda_k\cdot 1 &, l_{k-1} \leq h < l_k \\
1 &, l_k \leq h 
\end{array}
\right. $$ 
} where \small{$ \lambda_j := \frac{h-l_{j-1}}{l_j-l_{j-1}} $} and $l_j$ are lag centres.
\vspace{4pt}

The \alert{bivariate spatio-temporal copula} density $c_{h,\Delta}(u,v)$ is a convex combination of bivariate spatial copula densities over time.
\end{frame}

\begin{frame}{The spatio-temporal vine}
\begin{figure}
\includegraphics[width=\textwidth]{can_Vine_cost.png}
\end{figure}
\end{frame}

\begin{frame}{The full density}
\small
\begin{align*}
c^{\mathbf{\Delta}}_{\mathbf{h}} (u_0, \dots,u_{d}) =& \prod\limits_{i=1}^{d} c^\Delta_{h(0,i)}(u_0,u_i) \\
&\cdot \prod\limits_{j=1}^{d-1}\prod\limits_{i=1}^{d-j}c_{j,j+i|0,\dots,j-1} (u_{j|0,\dots,j-1}, u_{j+i|0,\dots,j-1})
\end{align*}

where $u_i = F_i\big(Z(s_q,t_r)\big)$ for $0 \leq i \leq d$ with $(s_q,t_r)$ denoting the $i$-th strongest correlated neighbour of $(s_0,t_0)$ with $F_i=F_{q,r}$ and 

\begin{align*}
u_{i|0} &= F_{i|0}(u_i|u_0) = \frac{\partial C_{h(0,i)}^\Delta(u_0,u_i)}{\partial u_0}\\
u_{j+i|0,\dots, j-1} &= F_{j+i|0,\dots j-1}(u_{j+i}|u_0,\dots, u_{j-1})\\
	&=\frac{\partial C_{j-1,j+i|0,\dots j-2}(u_{j-1|0,\dots j-2},u_{j+i|0,\dots j-2})}{\partial u_{j-1|0,\dots j-2}}
\end{align*}
\end{frame}

\begin{frame}
\frametitle{Spatio-Temporal Vine-Copula interpolation}

The estimate can be obtained as the expected value

\begin{align*}
\widehat{Z}_m\big((s_0,t_0)\big) &= \int_\mathbb{R} z \cdot f^\mathbf{\Delta}_{\mathbf{h}}(z | z_1, \dots, z_d) \ \mathrm{d}z \\
&=\int_{[0,1]} F^{-1}(u) \ c^\mathbf{\Delta}_{\mathbf{h}}\big(u|u_1,\dots,u_d\big) \ \mathrm{d}u 
\end{align*}

or by calculating any percentile $p$ (i.e. the median)

$$ \widehat{Z}_p\big((s_0,t_0)\big) = F^{-1}\big({C^\mathbf{\Delta}_\mathbf{h}}^{-1}(p|u_1,\dots,u_d)\big)$$
\end{frame}

\begin{frame}{Applications}
\begin{description}
\item<1->[zinc] concentrations in the Meuse river floodplain, a spatial random field with GEV margins
\item<1->[daily mean $\rm{PM}_{10}$] rural background concentrations across Europe during 2005, a spatio-temporal random field with GEV margins \end{description}
\end{frame}

\subsection{Spatio-temporal co-variate}

\begin{frame}{Adding the Covariate}

\small $(Z,Y):\Omega\times S \times T \rightarrow \IR^2$ with margins $F_s$ and $G_s$.

\begin{figure}
\includegraphics[width=\textwidth]{can_Vine_co.png}
\end{figure}
\end{frame}

\begin{frame}{The full density}
\begin{align*}
& c^{\mathbf{\Delta}}_{\mathbf{h}} (u_0, {\color{blue52n}v_0}, u_1,\dots,u_{d})\\ 
=& {\color{blue52n}c_{ZY}(u_0,v_0)} \cdot \prod\limits_{i=1}^{d} c^\Delta_{h(0,i)}(u_0,u_i) 
\cdot {\color{blue52n}\prod\limits_{i=1}^{d}c_{Y,i|0} (u_{Y|0}, u_{i|0})}\\
&\cdot \prod\limits_{j=1}^{d-1}\prod\limits_{i=1}^{d-j}c_{j,j+i|{\color{blue52n}Y},0,\dots,j-1} (u_{j|{\color{blue52n}Y},0,\dots,j-1}, u_{j+i|{\color{blue52n}Y},0,\dots,j-1})
\end{align*}
\small
where {\color{blue52n}$v_0 = G_{0}\big(Y(s_0,t_0)\big)$}, $u_i = F_i\big(Z(s_q,t_r)\big)$ for $0 \leq i \leq d$ with $(s_q,t_r)$ denoting the $i$-th strongest correlated neighbour of $(s_0,t_0)$ with $F_i=F_{q,r}$ and 
{\color{blue52n}$$u_{Y|0} = F_{Y|0}(v_0|u_0) = \frac{\partial C_{ZY}(u_0,v_0)}{\partial u_0}$$} \dots
\end{frame}

\begin{frame}
\frametitle{Spatio-Temporal Covariate Vine-Copula interpolation}

The estimate can be obtained as the expected value

\begin{align*}
\widehat{Z}_m\big((s_0,t_0)\big) &= \int_\mathbb{R} z \cdot f^\mathbf{\Delta}_{\mathbf{h}}(z | {\color{blue52n}y_0}, z_1, \dots, z_d) \ \mathrm{d}z \\
&=\int_{[0,1]} F_0^{-1}(u) \ c^\mathbf{\Delta}_{\mathbf{h}}\big(u|{\color{blue52n}v_0}, u_1,\dots,u_d\big) \ \mathrm{d}u 
\end{align*}

or by calculating any percentile $p$ (i.e. the median)

$$ \widehat{Z}_p\big((s_0,t_0)\big) = F_0^{-1}\big({C^\mathbf{\Delta}_\mathbf{h}}^{-1}(p|{\color{blue52n}v_0},u_1,\dots,u_d)\big)$$
\end{frame}

\begin{frame}[allowframebreaks]{Application}
Daily mean $\rm{PM}_{10}$ rural background concentrations observed (PM10) and modelled (EMEP) across Europe during 2005 as co-variate, a bivariate spatio-temporal random field with spatially varying GEV margins.

\begin{figure}
\center
\includegraphics[width=\textwidth]{PM10_EMEP_correlation.pdf}
\end{figure}
\end{frame}
\begin{frame}{Application}
\begin{figure}
\center
\includegraphics[width=\textwidth]{correlogram_STCoVar.pdf}

%\includegraphics[width=\textwidth]{condPredCDF_STCoVar.pdf}
\end{figure}

\end{frame}

\begin{frame}{Cross validations $\rm{PM}_{10}$}
\begin{table}
\tabcolsep 1.8pt
\center\small
\begin{tabular}{ll|rrrr}
Dependence model & Margin & RMSE & MAE & ME & COR \\ \hline 
\rule{0pt}{14pt}STCV $\widehat{Z}_m$ & local GEV & \bf{8.53} & 4.61 & \bf{-0.05} & \bf{0.84} \\
Gaussian STCV $\widehat{Z}_m$  & local GEV & 8.65 & \bf{4.59} & 0.08 & 0.83 \\
STCV $\widehat{Z}_m$ & lm+IDW GEV& 10.12 & 5.79 & 0.17 & 0.76 \\
STCV $\widehat{Z}_m$ & IDW GEV&  10.82 & 6.26 & 0.14 & 0.72 \\
metric res. kriging & log linear reg. & 10.67 & 6.16 & 0.47 & 0.74 \\
sp.-temp. vine $\widehat{Z}_m$ & global GEV & 11.20 & 6.95 & -0.73 & NA 
\end{tabular}
\end{table}
\end{frame}

\subsection{Multiple tree spatial}

\begin{frame}{The spatial neighbourhood}
\begin{figure}
\includegraphics[width=\textwidth]{can_Vine.png}
\end{figure}
\end{frame}

\begin{frame}{The full density}

\begin{align*}
& c_{\mathbf{h}} (u_{0},\dots,u_{d}) \nonumber\\
=& \prod\limits_{i=1}^{d} c_{h_0({i})}(u_0,u_i) \cdot \prod\limits_{j=1}^{d-1}\prod\limits_{i=1}^{d-j}c_{\color{blue52n}h_{j}(j+i)} (u_{j|0,\dots,j-1}, u_{j+i|0,\dots,j-1})
\end{align*}
where $u_i = F_i\big(Z(s_i)\big)$ for $0 \leq i \leq d$ and
\begin{align*}
u_{j+i|0,\dots, j-1}&=F_{\color{blue52n}h_{j-1}(j+i)}(u_{j+i}|u_0,\dots, u_{j-1})\\
&=\frac{\partial C_{\color{blue52n}h_{j-1}(j+i)}(u_{j-1|0,\dots j-2},u_{j+i|0,\dots j-2})}{\partial u_{j-1|0,\dots j-2}}
\end{align*}\end{frame}

\begin{frame}{Spatial vine copula interpolation}
The estimate can be obtained as the expected value

$$ \widehat{Z}_m(s_0) = \int\limits_{[0,1]}  F^{-1}(u) \ c_{\mathbf{h}}\big(u|u_1,\dots,u_d\big) \ \mathrm{d}u $$

or by calculating any percentile $p$ (i.e. the median)

$$ \widehat{Z}_p(s_0) = F^{-1}\big(C_{\mathbf{h}}^{-1}(p|u_1,\dots,u_d)\big) $$
\end{frame}

\begin{frame}{Application}
Simulated accidental release of radioactivity (SIC2004),\\ 200 points to estimate 808.

\begin{figure}
\center
\includegraphics[width=\textwidth]{joker_hist.pdf}
\end{figure}
\end{frame}

\begin{frame}{The bivariate spatial copulas}
\vspace{-12pt}
\begin{figure}
\center
\animategraphics[loop,scale=0.2,poster=first,autoplay]{5}{plot}{11}{80}\\
\includegraphics[width=0.6\textwidth]{jokerSpCopula1}
\end{figure}
\end{frame}

\begin{frame}{Interpolated grid}
\begin{figure}
\includegraphics[width=0.75\textwidth]{qunatilePredWireFrame}
\end{figure}
\end{frame}

\begin{frame}\frametitle{Validation data set}
%808 locations have been hold back to assess the prediction quality.
\begin{table}
\center

\begin{tabular}{l|rrrr}
approach & MAE & RMSE & ME & COR \\ \hline
spatial vine copula &	\bf{14.5} &67.6	&-6.1	& 0.60 \\
TG log-kriging & 20.8 & 78.2 & -2.1 & 0.39 \\
residual kriging     & 21.1 & 75.6 & 5.2 & 0.43 \\
best one in SIC2004 & 14.9 & \bf{45.5} & \bf{-0.5} & \bf{0.84}  
\end{tabular}
\end{table}
\end{frame}

\begin{frame}{Reproduction of margins}
\begin{figure}
\includegraphics[width=\textwidth]{boxplots_emergency}
\end{figure}
\end{frame}

\begin{frame}{Uncertainty assessment}
\begin{figure}
\includegraphics[width=0.75\textwidth]{prediction_cdf_large}
\end{figure}

\begin{figure}
\includegraphics[width=0.75\textwidth]{prediction_cdf_small}
\end{figure}
\end{frame}

\section{Limitations \& Benefits}
\begin{frame}{Second tree's correlations}
\begin{figure}
\includegraphics[width=\textwidth]{condCorDistBoxed}
\end{figure}
\end{frame}

\begin{frame}{Implicit modelled correlations}
\begin{figure}
\includegraphics[width=0.5\textwidth]{can_Vine_implicit.png}

\includegraphics[width=\textwidth]{CorDistBoxed}
\end{figure}
\end{frame}

\begin{frame}\frametitle{Benefits}
\begin{description}
\item[richer flexibility] due to the various dependence structures
\item[asymmetric dependence structures] become possible (temporal direction)
\item[uncertainty assessment] becomes more realistic
\item[zero correlation] might still carry a valuable structure
\item[extremes] can be reproduced with skewed margins and the right dependence structure
\end{description}
\end{frame}

\section{Practical}

\begin{frame}{Tasks}
\begin{itemize}
\item fit a static Vine Copula to a multivariate sample
\item fit a spatial vine copula - along the lines of the demo("spCopula")
\end{itemize}
\end{frame}

\section{References}
\begin{frame}[fragile,allowframebreaks]{Key References}
\footnotesize
\begin{thebibliography}{}
\bibitem{Aas2009} Aas, K., C. Czado, A. Frigessi \& H. Bakken (2009): Pair-copula constructions of multiple dependence. \textit{Insurance: Mathematics and Economics}, 44, pp.182 - 198.
\bibitem{Bedford2002}Bedford, T. \& R.M. Cooke (2002): Vines -- a new graphical model for dependent random variables.
\textit{Annals of Statistics}, 30 (4), pp. 1031 - 1068.
\bibitem{Graler2014} Gr\"aler, B. (2014): Modelling Skewed Spatial Random Fields through the Spatial Vine Copula.
\textit{Spatial Statistics} (in press, available online).
\bibitem{Graler2013}Gr\"aler, B., M. J. van den Berg, S. Vandenberghe, A. Petroselli, S. Grimaldi, B. De Baets, \& N. E. C. Verhoest (2013): Multivariate return periods in hydrology: a critical and practical review focusing on synthetic design hydrograph estimation. 
\textit{Hydrology and Earth System Sciences}, 17, pp. 1281 - 1296.
\bibitem{Graler2012}Gr\"aler, B. \& E. Pebesma (2012): Modelling Dependence in Space and Time with Vine Copulas. \textit{Expanded Abstract Collection from Ninth International Geostatistics Congress}, Oslo Norway, June 11 - 15, 2012.
\bibitem{Graler2011}Gr\"aler, B. \& E. Pebesma (2011): The pair-copula construction for spatial data: a new approach to model spatial dependency. \textit{Procedia Environmental Sciences}, 7, pp. 206 - 211. 
\bibitem{HobaekHaff2010}Hob{\ae}k Haff, I., K. Aas \& A. Frigessi (2010): On the simplified pair-copula construction - Simply useful or too simplistic?. \textit{Journal of Multivariate Analysis}, 101 (5), pp. 1296 - 1310.
\bibitem{Nelsen2006} Nelsen, R. (2006). \textit{An Introduction to Copulas}. 2nd ed. New York: Springer.
\end{thebibliography}
\end{frame}
\end{document}