## How to Model Skewed and Heavy Tailed Distributions - an Introduction to Copulas

The slides and scripts have been composed for the half-day Workshop at the Spatial Statistics Conference 2019 in Sitges, Spain.

Author: [Dr. Benedikt Graeler](mailto:b.graeler@52north.org), 52°North Initiative for Geospatial Open Source Software GmbH 

- [Intro](https://bitbucket.org/ben_graeler/copula_workshop/raw/master/intro.pdf)

- [Background](https://bitbucket.org/ben_graeler/copula_workshop/raw/master/01_background.pdf)
- [Practical](https://bitbucket.org/ben_graeler/copula_workshop/raw/master/01_practical.R)

- [Introduction to Copulas](https://bitbucket.org/ben_graeler/copula_workshop/raw/master/02_IntroToCopulas.pdf)
- [Practical](https://bitbucket.org/ben_graeler/copula_workshop/raw/master/02_practical.R)

- [Spatial Copulas](https://bitbucket.org/ben_graeler/copula_workshop/raw/master/03_spatial_copulas.pdf)
- [Practical](https://bitbucket.org/ben_graeler/copula_workshop/raw/master/03_practical.R)

This Repository is licensed as CC BY-SA.